# LICENSE #

The library SDCLib/J (hereafter referred to as __this library__) is licensed under the GNU General Public License version 3 including the possibility of a non-commercial license exception as additional permission to the GPL (see terms below).

## Terms ##

Copyright (C) 2021 SurgiTAIX AG

This library is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program; if not, see <https://www.gnu.org/licenses>.

See the full text [here](https://www.gnu.org/licenses/gpl-3.0.en.html).

### License exception ###
Additional permission under GNU GPL version 3, section 7.

As a special exception, the copyright holder of this library grants you permission to link this library with independent modules to create a closed-source combined work, regardless of the license terms of those independent modules, and to copy and distribute the combined work under terms of your choice, provided that you also comply with the license terms of each linked independent module. An independent module is a module that is not derived from or based on this library. 

#### The application of this exception is subject to the following conditions ####

* The application of this exception is free of charge in the case of non-commercial use.
* In case of commercial use the application of this exception requires you to pay a fee to the copyright holder. Commercial use means that profit may be generated in the course of distribution of a combined work beyond mere cost recovery.

#### The following modification exceptions apply ####

* Modifications of this library itself must remain open-source in case of non-commercial distribution of a combined work.
* In case of commercial use the source-code of a modified version of this library may or may not be published in the course of distributing a combined work. It is however not allowed to redistribute modified closed-source versions of just this library, outside of the scope of combined work.

This license exception is valid, even if the terms of the GPL are violated. If you publish the source-code of modified versions of this library, you are obliged to to remove this exception from the terms of your version of the library, as you yourself become the copyright holder of your changes (see the contributions section below).

### Contributions ###

If you would like to contribute to this project, you may fork it and re-publish it yourself. In case you would like your contributions to become part of the original project, you can create a pull request to have your changes reviewed and eventually accepted for reintegration.

If you publish modified versions of this library, you may or may not remove this exception from your version of the library.

#### Transfer of copyright ####

By creating a pull request to the original project you agree to assign the copyright of the changes to the original project, to be licensed under the same terms as the original source-code.

## Contact ##

If you have any questions please contact office@surgitaix.com

